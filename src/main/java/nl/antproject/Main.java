package nl.antproject;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        String key = "a-key";
        Multimap<String, String> map = ArrayListMultimap.create();

        map.put(key, "firstValue");
        map.put(key, "secondValue");

        System.out.println(map);


    }

}
